# PLS.sh

A shell interpreter made in C in 2017.
This is an old project I'm importing manually so there is no commit history.

This interpreter handles a full environment and makes full use of the `$PATH` variable to find scripts.
It has a few built-ins namely `cd`, `echo`, `setenv` and `unsetenv`.
It handles redirections and pipes.
It has a command history.

The projest also has a config file `.PLSshrc` at the root of the project where you can specify your prefered prompt and aliases like so
```
set prompt = "PLSsh> "

alias nw emacs -nw
```

## Install and run
You can compile the project from within the repo using
```
make
```
This should create the `PLSsh` binary that you can run with
```
./PLSsh
```
The binary does not take any parameters and any excess parameter will result in it returning an error code.

## License
MIT license